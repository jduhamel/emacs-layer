(require 'gnus)

(provide 'gnus-config)


(setq user-mail-address "jduhamel@gmail.com")
(setq user-full-name "Joe Duhamel")

(setq mm-inline-large-images 'resize)
